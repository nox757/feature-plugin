package window;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.LangDataKeys;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VfsUtilCore;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.VirtualFileVisitor;
import org.jetbrains.annotations.NotNull;
import util.ExcelReader;

import java.io.IOException;
import java.util.Map;

public class WindowAction extends AnAction {


    @Override
    public void actionPerformed(@NotNull AnActionEvent event) {
        Project project = event.getProject();
        VirtualFile file = event.getData(LangDataKeys.VIRTUAL_FILE);

        MyDialogWrapper dialogWrapper = new MyDialogWrapper();
        boolean result = dialogWrapper.showAndGet();
        String filePath = dialogWrapper.getFilePath();
        String folderPath = dialogWrapper.getFolderPath();
        if (!folderPath.isEmpty()) {
            file = LocalFileSystem.getInstance().findFileByPath(folderPath);
        }
        if (result) {
            Map<String, String> map = new ExcelReader(filePath).readOldNewValues();
            VfsUtilCore.visitChildrenRecursively(file, new VirtualFileVisitor() {
                @Override
                public boolean visitFile(VirtualFile file) {
                    if (!file.isDirectory() && file.getName().endsWith(".feature")) {
                        Document currentDoc = FileDocumentManager.getInstance().getDocument(file);
                        WriteCommandAction.runWriteCommandAction(project, () -> {
                                    for (String oldTag : map.keySet()) {
                                        String newText = currentDoc.getText();
                                        if(newText.contains("@" + oldTag + " ")) {
                                            String newTag = map.get(oldTag);
                                            try {
                                                file.rename(event, file.getName().replace(oldTag, newTag));
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                                return;
                                            }
                                            newText = newText.replaceAll("@" + oldTag + " ", "@" + newTag + " ");
                                            currentDoc.setText(newText);
                                        }
                                    }
                                }
                        );


                    }
                    return true;
                }
            });
        }

    }

}
