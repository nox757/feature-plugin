package window;

import com.intellij.openapi.fileChooser.FileChooserDescriptor;
import com.intellij.openapi.fileChooser.FileChooserDescriptorFactory;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.TextFieldWithBrowseButton;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

public class MyDialogWrapper extends DialogWrapper {

    private TextFieldWithBrowseButton fieldWithFile;
    private TextFieldWithBrowseButton fieldWithFolder;


    public MyDialogWrapper() {
        super(true);
        init();
        setTitle("Test window.MyDialogWrapper");
//        JButton jButton = getButton(this.myOKAction);
//        jButton.setText("kkkkk");

    }

    @Nullable
    @Override
    protected JComponent createCenterPanel() {
        JPanel dialogPanel = new JPanel(new BorderLayout());

        JLabel label = new JLabel("testing");
        label.setPreferredSize(new Dimension(100, 100));
        dialogPanel.add(label, BorderLayout.CENTER);

        //choose file
        fieldWithFile = new TextFieldWithBrowseButton();
        FileChooserDescriptor descriptor = FileChooserDescriptorFactory.createSingleFileDescriptor();
        fieldWithFile.addBrowseFolderListener("CHOOSE FILE", null, null, descriptor);
        dialogPanel.add(fieldWithFile, BorderLayout.CENTER);

        //choose folder
        fieldWithFolder = new TextFieldWithBrowseButton();
        FileChooserDescriptor descriptor2 = FileChooserDescriptorFactory.createSingleFolderDescriptor();
        fieldWithFolder.addBrowseFolderListener("CHOOSE FOLDER APPLY", null, null, descriptor);
        dialogPanel.add(fieldWithFolder, BorderLayout.PAGE_END);

        return dialogPanel;
    }

    public String getFilePath() {
        String folderPath = fieldWithFile.getText();
        if(folderPath.isEmpty()) {
            return "";
        }
        return folderPath;
    }

    public String getFolderPath() {
        String folderPath = fieldWithFolder.getText();
        if(folderPath.isEmpty()) {
            return "";
        }
        return folderPath;

    }

}