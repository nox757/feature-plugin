package util;

import com.google.protobuf.MapEntry;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ExcelReader {

    public final String xlsPath;

    public ExcelReader(String xlsPath) {
        this.xlsPath = xlsPath;
    }

    public Map<String, String> readOldNewValues() {

        Map<String, String> values = new HashMap<>();

        try (Workbook workbook = WorkbookFactory.create(new File(xlsPath))) {
            Sheet sheet = workbook.getSheetAt(0);
            for (Row row : sheet) {
                values.put(row.getCell(0).getStringCellValue(), row.getCell(1).getStringCellValue());
            }
        } catch (InvalidFormatException | IOException e) {
            e.printStackTrace();
        }
        return values;
    }

    public void writeNewValues(Map<String, String> values) {

        Workbook book = new HSSFWorkbook();
        Sheet sheet = book.createSheet("Sheet 0");

        int i = 0;
        for (Map.Entry<String, String> entry : values.entrySet()) {
            Row row = sheet.createRow(i++);
            row.createCell(0).setCellValue(entry.getKey());
            row.createCell(1).setCellValue(entry.getValue());
        }

        sheet.autoSizeColumn(1);

        try {
            book.write(new FileOutputStream(xlsPath));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                book.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}