import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.LangDataKeys;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VfsUtilCore;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.VirtualFileVisitor;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StepFileAction extends AnAction {

    public StepFileAction() {
        super("Set numeric step");
    }

    @Override
    public void actionPerformed(AnActionEvent event) {
        Project project = event.getProject();
        VirtualFile file = event.getData(LangDataKeys.VIRTUAL_FILE);
        VfsUtilCore.visitChildrenRecursively(file, new VirtualFileVisitor() {
            @Override
            public boolean visitFile(VirtualFile file) {
                if (!file.isDirectory() && file.getName().endsWith(".feature")) {
                    Document currentDoc = FileDocumentManager.getInstance().getDocument(file);
                    WriteCommandAction.runWriteCommandAction(project, () ->
                            currentDoc.setText(replaceSteps(currentDoc).concat("\n"))
                    );
                }
                return true;
            }
        });

    }


    public String replaceSteps(Document fileName) {
        final int[] i = {0};
        String regex = "(\\s*\\* шаг \")\\d*(\".*)";
        try (Stream<String> lines = Stream.of(fileName.getText().split("\n"))) {
            return lines
                    .map(line -> {
                        if (line.matches(regex)) {
                            i[0]++;
                        }
                        return line.replaceAll(regex, "$1" + i[0] + "$2");
                    })
                    .collect(Collectors.joining("\n"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return fileName.getText();
    }
}